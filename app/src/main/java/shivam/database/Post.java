package shivam.database;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Post extends AppCompatActivity {

    TextView t1;
    public SharedPreferences settings ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);
        t1 = (TextView) findViewById(R.id.t1);
        settings = getSharedPreferences("settings", MODE_PRIVATE);
        String str = settings.getString("edittext_str","DEFAULT");
        t1.setText(str);
    }
}
